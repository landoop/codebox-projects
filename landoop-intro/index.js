var path = require("path");

module.exports = {
    id: "landoop-intro",
    name: "Introduction to Landoop",

    sample: path.resolve(__dirname, "sample"),
    detector: path.resolve(__dirname, "detector.sh"),
    runner: [
        {
            id: "run",
            script: path.resolve(__dirname, "run.sh")
        }
    ]
};
