var path = require("path");

module.exports = {
    id: "social-media-analytics",
    name: "Social Media Analytics (Scala)",

    sample: path.resolve(__dirname, "sample"),
    detector: path.resolve(__dirname, "detector.sh")
};
